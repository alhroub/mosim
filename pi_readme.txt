Instructions for mosim on pi:
sudo apt-get update
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

## Install compilers and make if not installed
sudo apt-get update -y
sudo apt-get install -y make autoconf automake libtool

Install zeromq: 
wget http://Download.zeromq.org/zeromq-4.0.3.tar.gz
tar zxvf zeromq-4.0.3.tar.gz
cd zeromq-4.0.3/
sudo ./configure 
sudo make 
sudo make install 
sudo ldconfig

## npm install other components in the mozim directory
cd ..
sudo apt-get install npm
sudo npm install zmq
sudo npm install coap
sudo npm install packet
sudo npm install cbor-sync

