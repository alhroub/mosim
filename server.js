#!/usr/bin/env node
 
// listen for DCP commands on the cip server port


var config = require('./conf/config.js');
//var zmq    = require('zmq');
var packet = require('packet');
var coap   = require('coap');
var cbor   = require('cbor-sync');
var server = coap.createServer();
var lastTimeStamp = new Date();
var fs = require ('fs');
var logpath = './resourcelog.csv';
let deviceList = require ("./devices.json");

global.getCounter = 0;
global.putCounter = 0;
global.logCounter = 0;

var sensorBaseValue = 
    {
        "values" :
        [
        /* hour, als, aq, ct, power, co2, temp, hum, pir, conf*/
        {"hour":"0","als": "5","aq": "450","ct": "27","power": "20","co2": "150","temp": "11","hum": "5","pir": "50", "conf":"4"},
        {"hour":"1","als": "15","aq": "450","ct": "260","power": "25","co2": "150","temp": "12","hum": "4","pir": "50", "conf":"4"},
        {"hour":"2","als": "600","aq": "450","ct": "27","power": "4","co2": "150","temp": "12","hum": "10","pir": "50", "conf":"4"},
        {"hour":"3","als": "2000","aq": "1150","ct": "2800","power": "30","co2": "250","temp": "2","hum": "20","pir": "50", "conf":"9"},
        {"hour":"4","als": "4000","aq": "1800","ct": "2800","power": "30","co2": "250","temp": "3","hum": "20","pir": "50", "conf":"9"},
        {"hour":"5","als": "3000","aq": "510","ct": "2800","power": "25","co2": "250","temp": "15","hum": "40","pir": "50", "conf":"5"},
        {"hour":"6","als": "1500","aq": "550","ct": "3550","power": "15","co2": "250","temp": "1","hum": "40","pir": "50", "conf":"8"},
        {"hour":"7","als": "3000","aq": "450","ct": "4450","power": "10","co2": "350","temp": "0","hum": "50","pir": "50", "conf":"6"},
        {"hour":"8","als": "350","aq": "450","ct": "5050","power": "10","co2": "350","temp": "-10","hum": "60","pir": "50", "conf":"8"},
        {"hour":"9","als": "38","aq": "450","ct": "5500","power": "12","co2": "350","temp": "-20","hum": "80","pir": "50", "conf":"8"},
        {"hour":"10","als": "5400","aq": "450","ct": "5000","power": "20","co2": "350","temp": "-25","hum": "70","pir": "50", "conf":"15"},
        {"hour":"11","als": "2500","aq": "750","ct": "4550","power": "12","co2": "450","temp": "-30","hum": "80","pir": "50", "conf":"10"},
        {"hour":"12","als": "64000","aq": "750","ct": "6200","power": "15","co2": "450","temp": "-10","hum": "80","pir": "50", "conf":"5"},
        {"hour":"13","als": "6900","aq": "750","ct": "6100","power": "10","co2": "450","temp": "80","hum": "50","pir": "50", "conf":"5"},
        {"hour":"14","als": "55","aq": "1800","ct": "6950","power": "30","co2": "1050","temp": "100","hum": "40","pir": "50", "conf":"5"},
        {"hour":"15","als": "5300","aq": "1550","ct": "4850","power": "40","co2": "1050","temp": "-20","hum": "50","pir": "50", "conf":"10"},
        {"hour":"16","als": "48000","aq": "1250","ct": "7050","power": "10","co2": "1050","temp": "80","hum": "64","pir": "50", "conf":"10"},
        {"hour":"17","als": "32000","aq": "950","ct": "7300","power": "20","co2": "1050","temp": "50","hum": "60","pir": "50", "conf":"10"},
        {"hour":"18","als": "60000","aq": "950","ct": "7800","power": "35","co2": "650","temp": "-10","hum": "1","pir": "50", "conf":"5"},
        {"hour":"19","als": "23000","aq": "950","ct": "2850","power": "26","co2": "550","temp": "10","hum": "10","pir": "50", "conf":"5"},
        {"hour":"20","als": "2300","aq": "650","ct": "2850","power": "12","co2": "450","temp": "90","hum": "10","pir": "50", "conf":"5"},
        {"hour":"21","als": "2300","aq": "450","ct": "2750","power": "15","co2": "350","temp": "80","hum": "20","pir": "50", "conf":"5"},
        {"hour":"22","als": "65000","aq": "1200","ct": "2750","power": "10","co2": "250","temp": "100","hum": "20","pir": "50", "conf":"5"},
        {"hour":"23","als": "60000","aq": "1500","ct": "2750","power": "10","co2": "50","temp": "12","hum": "10","pir": "50", "conf":"5"}
        ]
    };
    


var newSim = [];
var Sim = { 
    SimulatorInfo: {
        Name: 'MoSim',
        TranscendServer: '192.168.1.10',
        IPAddress: '10.14.130.153',
        version: 'v1.0.4'
    },
    Devices: []
    };

/**
  * Handle log messages
  * @param {string} msg - log message
  * @global
  */
global.LOG = function (msg) {
  var stamp = new Date().toISOString().substr(5, 14).replace('T', ' ');
  console.log(stamp+" "+msg);
}
 
/** ZeroMQ DCP command socket for push requests on server port
  * @global */
//global.zcmd = zmq.socket('push');
//zcmd.connect('tcp://127.0.0.1:'+ config.port.server);
//LOG('ZeroMQ command socket created on port '+config.port.server);
 
/**  ZeroMQ DCP subscribe socket on status port */
//var zsub = zmq.socket('sub');
//zsub.connect('tcp://127.0.0.1:'+config.port.status);
//zsub.subscribe('');
//LOG('ZeroMQ status socket listening on port '+config.port.status);
 

// global UIDs
var SYSUID = "Lighting-System";
var COAPUID = "COAP-Adapter";


//setup timers
setInterval(pirSensorLoader, 300000);

function addNewNode(id, type)
{
 
    if(type == "light")
    {
     
    }
    else if(type == "sensor")
    {
        
    }
}

function write_logTofile(text) {
    var stamp = new Date().toISOString().substr(5, 14).replace('T', ' ');
    var ntext = stamp+", " + text +"\n";
    //console.log (ntext);
    fs.writeFile(logpath, ntext, function (err) {
    if (err) 
        return console.log(err);
    });
}

function append_logTofile(text) {
    var stamp = new Date().toISOString().substr(5, 14).replace('T', ' ');
    var ntext = stamp +", " + text ;
    //console.log (ntext);
    fs.appendFile(logpath, ntext, function (err) {
    if (err) 
        return console.log(err);
    });
}

function logPUTtoResource (resName, pi, vi, at){
    global.logCounter++;
    
    var logString = resName + ", " + pi +  ", " + vi + ", " + at + "\n";
    if(global.logCounter > 25000) {
        global.logCounter = 0;
        write_logTofile(logString);
    }
    else {
        append_logTofile(logString);
    }
}

/**
  * Initialize our DCP objects
  */
function init() {
    

   LOG ("MoSim starting ");
    LOG ("Simulator version " + Sim.SimulatorInfo.version +" \n");
    
    write_logTofile ("MoSim session starting ..\n");
    append_logTofile ("Simulator version " + Sim.SimulatorInfo.version + "\n");
    append_logTofile ("Time,    Name, pi, vi, at" + Sim.SimulatorInfo.version + "\n");
    
    
    fs.appendFile(logpath, "Simulator version \n" + Sim.SimulatorInfo.version, function (err) {
    if (err) 
        return console.log(err);
        
    });
    
    //var last = Sim.Device.length;
    
//    Sim.Devices = ;
    
    var index = 0;
    var lineItem = "";
    var lightCount = 0;
    var sensorCount = 0;
    for (var l in deviceList.lights)
    {
        lineItem = {
            "NAME" :  deviceList.lights[l].id,    
            "DeviceType":  deviceList.lights[l].type,
            "SubType": "Troffer",
            "pi":"0",
            "vi":"0",
            "at":"0",
            "lastUpdate" : "0",
            "lastStamp" : "0"
        };
        
        Sim.Devices.push(lineItem);
        LOG ("Pushing " + lineItem.NAME+ ":" +lineItem.DeviceType + ":" + lineItem.SubType);
        lightCount++;
    }

    for (var r in deviceList.rgbw) {
        lineItem = {
            "NAME": deviceList.rgbw[r].id,
            "DeviceType": deviceList.rgbw[r].type,
            "SubType": "Beacon",
            "pi": "0",
            "vi": "0",
            "at": "0",
            "lastUpdate": "0",
            "lastStamp": "0"
        };
        Sim.Devices.push(lineItem);
        LOG("Pushing " + lineItem.NAME + ":" + lineItem.DeviceType + ":" + lineItem.SubType);
        lightCount++;

    }

    
    for (var s in deviceList.sensors)
    {
        
        if(deviceList.sensors[s].subtype == "als"){
            lineItem = {
                "NAME" :  deviceList.sensors[s].id,    
                "DeviceType":  deviceList.sensors[s].type,
                "SubType" : "ALS",
                "pi":"0",
                "vi":"0",
                "at":"0",
                "lastUpdate" : "0",
                "lastStamp" : "0"
            };
        }
        else if(deviceList.sensors[s].subtype == "pir"){
            lineItem = {
                "NAME" :  deviceList.sensors[s].id,    
                "DeviceType":  deviceList.sensors[s].type,
                "SubType" : "PIR",
                "pi":"0",
                "vi":"0",
                "at":"0",
                "lastUpdate" : "0",
                "lastStamp" : "0"
            };
        }
        else if(deviceList.sensors[s].subtype == "aq"){
            lineItem = {
                "NAME" :  deviceList.sensors[s].id,    
                "DeviceType":  deviceList.sensors[s].type,
                "SubType" : "AQ",
                "pi":"0",
                "vi":"0",
                "at":"0",
                "lastUpdate" : "0",
                "lastStamp" : "0"
            };
        }
        else if(deviceList.sensors[s].subtype == "ct"){
            lineItem = {
                "NAME" :  deviceList.sensors[s].id,    
                "DeviceType":  deviceList.sensors[s].type,
                "SubType" : "CT",
                "pi":"0",
                "vi":"0",
                "at":"0",
                "lastUpdate" : "0",
                "lastStamp" : "0"
            };
        }
        else if(deviceList.sensors[s].subtype == "temp"){
            lineItem = {
                "NAME" :  deviceList.sensors[s].id,    
                "DeviceType":  deviceList.sensors[s].type,
                "SubType" : "TEMP",
                "pi":"0",
                "vi":"0",
                "at":"0",
                "lastUpdate" : "0",
                "lastStamp" : "0"
            };
        }
        else if(deviceList.sensors[s].subtype == "hum"){
            lineItem = {
                "NAME" :  deviceList.sensors[s].id,    
                "DeviceType":  deviceList.sensors[s].type,
                "SubType" : "HUM",
                "pi":"0",
                "vi":"0",
                "at":"0",
                "lastUpdate" : "0",
                "lastStamp" : "0"
            };
	  }
          else if(deviceList.sensors[s].subtype == "power"){
            lineItem = {
                "NAME" :  deviceList.sensors[s].id,
                "DeviceType":  deviceList.sensors[s].type,
                "SubType" : "POWER",
                "pi":"0",
                "vi":"0",
                "at":"0",
                "lastUpdate" : "0",
                "lastStamp" : "0"
            };

        }
            
        Sim.Devices.push(lineItem);
        sensorCount++;
        LOG ("Pushing " + lineItem.NAME+ ":" +lineItem.DeviceType + ":" + lineItem.SubType);    
    }

    LOG ("MoSim running ... ");
    LOG ("Lights: " + lightCount);
    LOG ("Sensors: " + sensorCount);
    
}
 
/**
  * Convert uint16 to int
  */
function i16(num) {
  var b = new ArrayBuffer(2);
  var v = new DataView(b);
  v.setUint8(0, num[0]);
  v.setUint8(1, num[1]);
  return v.getUint16(0);
}
 
/**
  * Convert uint32 to int
  */
function i32(num) {
  var b = new ArrayBuffer(4);
  var v = new DataView(b);
  v.setUint8(0, num[0]);
  v.setUint8(1, num[1]);
  v.setUint8(2, num[2]);
  v.setUint8(3, num[3]);
  return v.getUint32(0);
}
 
/**
  * Convert int to int16
  */
function toInt16(num) {
  var b = new ArrayBuffer(2);
  var v = new DataView(b);
  var d = new Uint8Array(b);
  v.setInt16(0, num);
  return [ d[0], d[1] ];
}
 
/**
  * Convert int to int32
  */
function toInt32(num) {
  var b = new ArrayBuffer(4);
  var v = new DataView(b);
  var d = new Uint8Array(b);
  v.setInt32(0, num);
  return [ d[0], d[1], d[2], d[3] ];
}
 
/**
  * Convert int to uint16
  */
function toUint16(num) {
  var b = new ArrayBuffer(2);
  var v = new DataView(b);
  var d = new Uint8Array(b);
  v.setUint16(0, num);
  return [ d[0], d[1] ];
}
 
/**
  * Convert int to uint32
  */
function toUint32(num) {
  var b = new ArrayBuffer(4);
  var v = new DataView(b);
  var d = new Uint8Array(b);
  v.setUint32(0, num);
  return [ d[0], d[1], d[2], d[3] ];
}
   
function decode_payload(str) {
  var ret = {};
  var p;

  if (typeof str == 'string') {
    p = str.split(',');
  }
  else {
    p = [];
    for (var k in str.e) {
      p.push(k);
      p.push(str.e[k]);
    }
  }
  for(var i=0; i<p.length; i+=2) {
    var v = p[i+1];
    switch (p[i]) {
      case '': break;  // just skip the first empty value
      case 'uuid': ret['UID'] = v.split('-').pop(); break;
      case 'n': ret['NAME'] = v.split('_',2).pop(); break;
      case 'v': 
      case 'fv': 
      case 'vi': if (ret['VI'] == undefined) {ret['VI'] = parseInt(v);} break; 
      case 'iv': if (ret['LEVEL'] == undefined) { ret['LEVEL'] = parseInt(v)}; break; 
      case 'vs':
      case 'sv': ret['LEVEL'] = v; break; 
      case 'vb':
      case 'bv': ret['LEVEL'] = (v == true); break; 
      case 'pi': ret['LEVEL'] = Math.round(v*1.0); break; 
      case 'm': ret['SCALE'] = v; break;
      case 'at': ret['AT'] = v; break;
      default: //console.log("  Skipping non-mapped coap parameter: "+p[i]);
    }
  }
  
  
  return ret;
}

function get(uid) {
    return Sim.Device[uid];
}

// Handle COAP sent to us
// incoming messages handled here 
server.on('request', function(req, res) {
  
  //LOG('Recv: '+req.method+' - '+req.url);
 
  var cmd = req.method.toString();
  switch (cmd) {
    case 'GET': cmd = cmd; break;
    case 'POST':
    case 'PUT': cmd = "SET"; break;
    default: console.log("Unsupported CoAP method: "+cmd);
  }
  var args = req.url.split('/');
  var opts = req.options;
  var payload;
  
  if (req.payload && req.payload.length > 0) {
    //LOG ("PAYLOAD Found");
      // see if the first character is a ',' or a '/'
    if (req.payload[0] == 44 || req.payload[0] == 47) {
        //LOG ("There is a plaintext payload");
      payload = req.payload.toString();
    }
    else {
    //    LOG ("There is a CBOR payload");
      payload = cbor.decode(req.payload);
    }
  }
   
  var a = args;
  for (var o in opts) {
    if (o.name == 'Uri-Path') {
      a = ['', '', o.value.toString()];      
      break;
    }
  }
  //LOG ("SERVER.ON - [" + req.method+"]  "+ "URL: " + req.url);
  var ret = process_coap(cmd, a, payload);

    // LOG ("Setting Content-Type to application/cbor");
    if(a[2] == "core") {
        res.setOption("Content-Format", "application/link-format");
    }
    else {
        res.setOption("Content-Type", "application/cbor");
        res.setOption('Block2', new Buffer([0x2]))
    }
    
  // trim bogus header info
  ret = ret.slice(5);
  res.end(ret)
})
  

//msk - TODO
function process_coap (cmd, args, payLoad) {
  var obj = {};
  
  if (payLoad) {
    obj = decode_payload(payLoad);
      
    logPUTtoResource(args[3], obj['LEVEL'], obj['VI'], obj['AT']);
    var name = args[3].substr(0);
 
      for (var d in Sim.Devices) {
        if (Sim.Devices[d].NAME == name) {
            
            Sim.Devices[d].at = obj['AT'];
            Sim.Devices[d].pi = obj['LEVEL'];
            Sim.Devices[d].vi = obj['VI'];
            Sim.Devices[d].lastStamp = new Date();
          break;
        }
      }
    }
   
 
  var device = process_coap_message(cmd, args, obj, COAPUID);
  return generate_response(device);
}
var setCounter = 0;

function process_coap_message (cmd, args, obj, uid) {
    
        
    if (cmd == 'GET') {
        if (args[3]) {       
            var name = args[3].substr(0);
            for (var d in Sim.Devices) {
                if (Sim.Devices[d].NAME == name) {     
                    break;
                }
            }
            var obj = Sim.Devices[d]; 
            global.getCounter++;
            if(  ( global.getCounter % 500) == 0) 
		LOG("[GET] " + args[3] + ". TSL: " + obj.lastUpdate + "s"+", Global Get Count: " + global.getCounter);
            return obj;
        }
        else if (args[2] == 'core') {
        
            // handle core end points
            var devices = '';
            LOG ("[CORE] args = " + args);
            for (var d in Sim.Devices) {
                var name = Sim.Devices[d].NAME;
                if (Sim.Devices[d].DeviceType == 'sensor') {
                    devices += '</molex/sensors/'+name+'>;title="'+name+'",';
                }
                else if (Sim.Devices[d].DeviceType == 'rgbw') {
                    devices += '</molex/actuators/' + name + '>;title="' + name + '",';
                } 
                else if (Sim.Devices[d].DeviceType == 'light') {
                    devices += '</molex/actuators/'+name+'>;title="'+name+'",';
                }
                if (ret.length > 1200){
                    LOG ("breaking ...");
                }
            }
            devices += '</.well-known/core>';
            
            LOG ("ret = " + devices);
        return devices;
        }
 
    console.log('unknown GET request');
    return '';
  }
  else if (cmd == 'SET') {
      newTimeStamp = new Date();
      var diff = newTimeStamp - lastTimeStamp;
      lastTimeStamp = newTimeStamp;
      setCounter ++;
      
      var name = args[3].substr(0);
        for (var d in Sim.Devices) {
            if (Sim.Devices[d].NAME == name) {     
                break;
            }
        }
        var obj = Sim.Devices[d];

      global.putCounter++;
      diff /=1000;
      obj.lastUpdate = diff;
      obj.lastStamp = newTimeStamp;

if(  ( global.getCounter %500) == 0)    LOG("                         [PUT] " + args[3] + ". TSL: " + obj.lastUpdate + "s"+", Global Get Count: " + global.getCounter);
                  
    return process_coap_message ('GET', args, obj, uid);
  }
 
}

/**
  * Send a response to the CoAP adapter
  * @param {object} obj - coap object to pack and send
  */
function generate_response(device) {
  var packet = require('coap-packet');
  var generate = packet.generate;

  var ret = create_payload(device);
  if (true){ // Always return in CBOR 
//        LOG ("Now encode in CBOR");
    var r = {};
    r.e = ret;
    ret = cbor.encode(r);
  }
  else {
//      console.log ("MO SAYS encode in PLAIN TEXT");
    ret = new Buffer(ret);
  }
  var message = generate({ payload: ret });
 
  return message;
}

function getNewPowerValue(low, hight){

    var objDate = new Date();
    var hours = objDate.getHours();
   var val = Math.floor(Math.random() * (parseInt(sensorBaseValue.values[hours].conf))) + parseInt(sensorBaseValue.values[hours].power);
  // console.log ("Power request: "  +  val + "\n");
return val;
}
function getNewALSValue(low, hight){
    
    var objDate = new Date();
    var hours = objDate.getHours();
  
   var val = Math.floor(Math.random() * (parseInt(sensorBaseValue.values[hours].conf))) + parseInt(sensorBaseValue.values[hours].als);

return val;


    if(hours >= 7 && hours < 8){
        var val = Math.floor(Math.random() * (15) + 300);
    }
    else if (hours >= 8 && hours <= 9){
        var val = Math.floor(Math.random() * 15 + 400);
    }
    else if (hours >= 9 && hours <= 11){
        var val = Math.floor(Math.random() * 15 + 500);
    }
    else if (hours >= 11 && hours <= 13){
        var val = Math.floor(Math.random() * 20 + 600);
    }
    else if (hours >= 13 && hours <= 15){
        var val = Math.floor(Math.random() * 25 + 600);
    }
    else if (hours >= 15 && hours <= 16){
        var val = Math.floor(Math.random() * 30 + 700);
    }
    else if (hours >= 16 && hours <= 18){
        var val = Math.floor(Math.random() * 30 + 500);
    }
    else if (hours >= 18 && hours <= 23){
        var val = Math.floor(Math.random() * 10 + 300);
    }
    else {
        var val = Math.floor(Math.random() * 10 + 100);
    }
    
  return val;
}

function getNewCTValue(low, hight){
    
    var objDate = new Date();
    var hours = objDate.getHours();

var val = Math.floor(Math.random() * (parseInt(sensorBaseValue.values[hours].conf))) + parseInt(sensorBaseValue.values[hours].ct);
//console.log ("als = " + val + " \n");
  return val;
}


function getNewAQValue(low, hight){
    
        var objDate = new Date();
	var hours = objDate.getHours();
	//LOG ("hours = " + hours);
    
	var val = Math.floor(Math.random() * (parseInt(sensorBaseValue.values[hours].conf))) + parseInt(sensorBaseValue.values[hours].aq);
	return val;

}

function getNewTempValue(low, hight){
    
    var objDate = new Date();
    var hours = objDate.getHours();
    
var val = Math.floor(Math.random() * (parseInt(sensorBaseValue.values[hours].conf))) + parseInt(sensorBaseValue.values[hours].temp);
//console.log ("Temp = " + val + " \n");
return val;
    
}

function getNewHumValue(low, hight){
    
    var objDate = new Date();
    var hours = objDate.getHours();

var val = Math.floor(Math.random() * (parseInt(sensorBaseValue.values[hours].conf))) + parseInt(sensorBaseValue.values[hours].hum);
return val;
}


function pirSensorLoader()
{
    var ts = new Date().valueOf()
    var val = Math.floor(Math.random() * 20);   
    if(val>8) global.PirValue = 1; else  global.PirValue=0;

}
global.PirValue = 0;

global.PirCount = 0;
function getNewPIRValue(){
    var occ = 1;    
    global.PirCount++;
    if(global.PirCount > 3000){
        var val = Math.floor(Math.random() * 20);
    
        if (val > 5)
           occ = 1 ;
        else 
            occ = 1;
        global.PirCount =0;
    }

occ = global.PirValue;
//console.log ("pir = " + global.PirValue + " \n");
 return occ;
}


function create_payload(device) {
    var ret = {};
    var r = '';
    var nidx = 0;
    //LOG ("create_payload");
 
    if (typeof device == 'object') {
        if (device.DeviceType == 'light') {
          ret['uuid'] = '2001-17-5410ec01d6e4';
          ret['n'] = +device['NAME'];
          ret['u'] = 'K';
          ret['cl'] = 'color';
          ret['m'] = '0';
          ret['a'] = '100';
          ret['ca'] = '10';
          ret['pi'] = device.pi;       
          ret['at'] = device.at;       
          //ret['vi'] = obj.vi;

        }
        if (device.DeviceType == 'rgbw') {
            ret['uuid'] = '2002-17-5410ec01d019';
            ret['n'] = device['NAME'];
            ret['u'] = 'rgbw';
            ret['cl'] = 'color';
            ret['m'] = '0';
            ret['a'] = '100';
            ret['ca'] = '10';
            ret['pi'] = device.pi;
            ret['at'] = device.at;
            ret['vi'] = device.vi;

        }
        if (device.DeviceType == 'sensor') {
            switch (device.SubType) {
                case 'ALS': {
                    ret['u'] = 'lx'; 
                    ret['vi'] = getNewALSValue(400, 600);
                    break;
                }
                case 'PIR': {
                    ret['u'] = "bv",
                    ret['cl'] = 'boolean',
                    ret['m'] = '0',
                    ret['a'] = '100',
                    ret['ca'] = '0'
                    //ret['u'] = 'boolean'; 
                    ret['bv'] = getNewPIRValue();
                    break;
                }
                case 'AQ': {
                    ret['u'] = 'ppm';
                    ret['vi'] = getNewAQValue();
                    break;
                }
                case 'CT':{
                    ret['u'] = 'K';
                    ret['vi'] = getNewCTValue();
                    break;
                }
                case 'TEMP':{
                    ret['u'] = 'C';
                    ret['vi'] = getNewTempValue();
                    break;
                }
                case 'HUM':{
                    ret['u'] = 'RH';
                    ret['vi'] = getNewHumValue();
                    break;
                }        
                case 'POWER':{
                    ret['u'] = 'WATT';
                    ret['vi'] = getNewPowerValue();
			break;
		}
                case 'CO sensor': ret['u'] = 'ppm'; break;
                case 'H2S sensor': ret['u'] = 'ppm'; break;
                case 'motion sensor': ret['u'] = 'boolean'; break;
            }
           
        }
 
        for (var param in device) {
            switch (param) {
                case 'CARB_MONOX':  if (device.DeviceType == 'sensor') {
                                      ret['vi'] = device['CARB_MONOX']; break;
                                    }
                case 'HYDR_SULF':   if (device.DeviceType == 'sensor') {
                                      ret['vi'] = device['HYDR_SULF']; break;
                                    }
                case 'MOTION':      ret['vb'] = obj['MOTION']; break;
                case 'TEMPERATURE': if (device.DeviceType == 'sensor') {
                                      ret['vi'] = device['TEMPERATURE']; break;
                                    }
                case 'LEVEL':       if (device.DeviceType == 'light') {
                                      ret['pi'] = Math.round((ret['vi']/255)*100); 
                                    }
                                    ret['vi'] = "4241";// device['LEVEL']; 
                                    break; 
                default:
            }
        }
        var newTimeStamp = new Date();
        var diff = newTimeStamp - device.lastStamp;
        device.lastStamp = newTimeStamp;
        device.lastUpdate = diff/1000;
        r = ret;
  }
  else {
        r = device;
  }
 
  return r;
}
// the default CoAP port is 5683 
server.listen();

global.send_err = function (err) {
  var errmsg = "COAP Err, " + err;
  LOG('Error: ' + errmsg);
}
 
init();
