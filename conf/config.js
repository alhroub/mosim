// logging level, 0 (off) - 10 (all)
var log = new Object;
log.log_level = 5;
log.log_length = 75;

// web server configuration
var web = new Object;
web.address   = "";
// DNS name or ip to access the server
web.port      = 80;
// external web port

// communication ports for the server
var port = new Object;
port.server   = 9001;
// localhost port
port.status   = 9002;
// localhost port
port.command  = 9003;
// localhost port
port.cron     = 9005;
// localhost port
port.notify   = 9006;
// localhost port
port.stepdown = 9007;
// localhost port
port.adr      = 9008;
// localhost port


// email notification settings
var email = new Object;
email.service = 'Gmail';
// Gmail or snmp mail service
email.host = 'localhost';
// snmp host server (not needed for gmail)
email.port = 25;
// snmp port number (not needed for gmail)
email.from = 'DACS Notify <dialight.dacs@gmail.com>';
email.username = 'dialight.dacs@gmail.com';
email.password = 'DaxT3st3r';


// text message settings
var text = new Object;
text.twilioSid = 'ACa69898dea2f2ccc91b26231a43a291b1';
text.twilioToken = 'db036dbe6ba6220b08b92ff141127ce8';
text.twilioFrom = "+18706191449";
// twilio phone number to text from


// twitter api settings
var twitter = new Object;
twitter.consumer_key    = '77Edf0r1CPXrj045P3wErMOTT',
twitter.consumer_secret = 'iXm8qaVJi23KyfpNsjMJmErWV2hyjTVhaW76P386VFgqeM2ufs',
twitter.token           = '2981511328-VMUyWbqs2SDNzHGAecE7XOn8UyoD6KLc2sMdCZf',
twitter.token_secret    = 'PsVOgsnJBwZZxUW7zv19wTlNNAkaK85Vgq1K6z4QZ1qE0'


//  directory references
var paths = {
  "backups"        : 'backups',
  "backupExt"      : 'tgz',
  "backupInfoExt"  : 'json',
  "backupTmp"      : 'tmp/backup',
  "upgradeWeb"     : 'upgrade/',
  "upgradeDir"     : 'www/upgrade/',
  "upgradeInfoExt" : 'json',
  "root"           : ''
};


// get defaults for unset values
if (!web.address) { web.address = find_local_ip() };
if (!web.port)    { web.port = 80; };


// helper functions
function find_local_ip() {
  var os = require('os');
  var ifaces = os.networkInterfaces();

  for (var net in ifaces) {
    for (var i = 0; i < ifaces[net].length; i++ ) {
      if (ifaces[net][i].family !== 'IPv4' || 
          ifaces[net][i].internal !== false) { continue; }
      return ifaces[net][i].address;
    }
  }
}


exports.log      = log;
exports.web      = web;
exports.port     = port;
exports.email    = email;
exports.text     = text;
exports.twitter  = twitter;
exports.paths    = paths;
