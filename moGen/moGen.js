#!/usr/bin/env node
 
// listen for DCP commands on the cip server port
var lastTimeStamp = new Date();
var fs = require ('fs');
var logpath = './devices.json';
let config = require ("./config.json");

var newSim = [];
var Sim = { 
    SimulatorInfo: {
        Name: 'MoSim',
        TranscendServer: '192.168.1.10',
        IPAddress: '10.14.130.153',
        version: '0.2'
    },
    Devices: []
    };

/**
  * Handle log messages
  * @param {string} msg - log message
  * @global
  */
global.LOG = function (msg) {
  var stamp = new Date().toISOString().substr(5, 14).replace('T', ' ');
  console.log(stamp+" "+msg);
}

function write_logTofile(text) {
    var ntext =text;
    fs.writeFile(logpath, ntext, function (err) {
    if (err) 
        return console.log(err);
    });
}

function append_logTofile(text) {
    var ntext =text ;
    fs.appendFile(logpath, ntext, function (err) {
    if (err) 
        return console.log(err);
    });
}

function logPUTtoResource (resName, pi, vi, at){
    
    var logString = resName + ", " + pi +  ", " + vi + ", " + at + "\n";
    append_logTofile(logString);
}

/**
  * Initialize our DCP objects
  */
function init() {
    

   LOG ("moGen starting ");
   var lightCount = parseInt(config.lights);
       
    LOG ("Creating " + lightCount +" molex troffers");
    LOG ("Creating " + config.als +" AL sensors");
        
    write_logTofile ("{\n");
    append_logTofile ("\"lights\": [ \n");
    
    var index=0;
    for(i = 0; i < lightCount-1; i++){
        var devName = "{\"id\":\"d"+i+".1_TrofferLight\", \"type\":\"light\"},\n";
        append_logTofile(devName);
        index=i;
    }
    // write last line in array
    var devName = "{\"id\":\"d"+index+".1_TrofferLight\", \"type\":\"light\"}\n";
    append_logTofile(devName);
    append_logTofile("],\n");
    
    append_logTofile ("\"sensors\": [ \n");
    index=0;
    devName = "{\"id\":\"d"+index+".1_ALS\", \"type\":\"sensor\"}\n";
    append_logTofile(devName);
    append_logTofile("]\n}");
    return;
    
    fs.appendFile(logpath, "Simulator version \n" + Sim.SimulatorInfo.version, function (err) {
    if (err) 
        return console.log(err);
        console.log('Wrote Hello World in file helloworld.txt, just check it');
    });
    
    //var last = Sim.Device.length;
    
//    Sim.Devices = ;
    
    var index = 0;
    for (var l in deviceList.lights)
    {
        Sim.Devices.push({
            "NAME" :  deviceList.lights[l].id,    
            "DeviceType":  deviceList.lights[l].type,
            "value":"0"});
    }

    for (var s in deviceList.sensors)
    {
        Sim.Devices.push({
            "NAME" :  deviceList.sensors[s].id,    
            "DeviceType":  deviceList.sensors[s].type,
            "value":"0"});
    }

    for (var l in Sim.Devices)
    {
        LOG ("Found " + Sim.Devices[l].NAME + " , type: " + Sim.Devices[l].DeviceType + " value: " + Sim.Devices[l].value);
    }
}
 
/**
  * Convert uint16 to int
  */
function i16(num) {
  var b = new ArrayBuffer(2);
  var v = new DataView(b);
  v.setUint8(0, num[0]);
  v.setUint8(1, num[1]);
  return v.getUint16(0);
}
 
/**
  * Convert uint32 to int
  */
function i32(num) {
  var b = new ArrayBuffer(4);
  var v = new DataView(b);
  v.setUint8(0, num[0]);
  v.setUint8(1, num[1]);
  v.setUint8(2, num[2]);
  v.setUint8(3, num[3]);
  return v.getUint32(0);
}
 
/**
  * Convert int to int16
  */
function toInt16(num) {
  var b = new ArrayBuffer(2);
  var v = new DataView(b);
  var d = new Uint8Array(b);
  v.setInt16(0, num);
  return [ d[0], d[1] ];
}
 
/**
  * Convert int to int32
  */
function toInt32(num) {
  var b = new ArrayBuffer(4);
  var v = new DataView(b);
  var d = new Uint8Array(b);
  v.setInt32(0, num);
  return [ d[0], d[1], d[2], d[3] ];
}
 
/**
  * Convert int to uint16
  */
function toUint16(num) {
  var b = new ArrayBuffer(2);
  var v = new DataView(b);
  var d = new Uint8Array(b);
  v.setUint16(0, num);
  return [ d[0], d[1] ];
}
 
/**
  * Convert int to uint32
  */
function toUint32(num) {
  var b = new ArrayBuffer(4);
  var v = new DataView(b);
  var d = new Uint8Array(b);
  v.setUint32(0, num);
  return [ d[0], d[1], d[2], d[3] ];
}

init();
